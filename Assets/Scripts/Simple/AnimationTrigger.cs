﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleBool : MonoBehaviour
{
    public bool pressed = false;
    private Animator anim;
  
    void Start()
    {
        anim = GetComponent<Animator>();
    }
    // Update is called once per frame
    void Update()
    {
        if (pressed == true)
        {
            if (anim != null)
            {
                anim.SetTrigger("pressed");
            }
            
        }
    }
}
