﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPart2Activator : MonoBehaviour
{
    public bool OpenEyes;
    private Animator anim;

    void Start()
    {
        anim = GetComponent<Animator>();
    }
  
    // Update is called once per frame
    void Update()
    {
        if (OpenEyes == true)
        {
            if (anim != null)
            {
                anim.SetTrigger("OpenEyes");
            }
        }
    }
}
