﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenEyesActivator : MonoBehaviour
{
    public IntroPart2Activator Open_Eyes_Activator;
    public bool pressed=false;

    void Update()
    {
        if (pressed == true)
        {
            Open_Eyes_Activator.OpenEyes = true;
        }
    }

 
}
