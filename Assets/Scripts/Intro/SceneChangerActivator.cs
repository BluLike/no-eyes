﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AKAGF.GameArchitecture.MonoBehaviours.Interaction
{
    public class SceneChangerActivator : MonoBehaviour
    {
        public Interactable someScript;
        public bool ChangeScene = false;
       

        // Update is called once per frame
        void Update()
        {
            if (ChangeScene == true)
            {
                someScript.Interact();
            }
        }
    }
}