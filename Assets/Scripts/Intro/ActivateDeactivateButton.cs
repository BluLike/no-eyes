﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateDeactivateButton : MonoBehaviour
{
    public bool Activated;
    public GameObject Object_1;
    public GameObject Object_2;
    public GameObject Object_3;
    public GameObject Object_4;
    public GameObject Object_5;


    void Update()
    {
        if (Activated == true)
        {
            Object_1.gameObject.SetActive(true);
            Object_2.gameObject.SetActive(true);
            Object_3.gameObject.SetActive(true);
            Object_4.gameObject.SetActive(true);
            Object_5.gameObject.SetActive(true);
        }
        else
        {
            Object_1.gameObject.SetActive(false);
            Object_2.gameObject.SetActive(false);
            Object_3.gameObject.SetActive(false);
            Object_4.gameObject.SetActive(false);
            Object_5.gameObject.SetActive(false);

        }
    }

  
}
