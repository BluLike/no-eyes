﻿using AKAGF.GameArchitecture.ScriptableObjects.Interaction.Conditions;
using AKAGF.GameArchitecture.ScriptableObjects.Variables;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace AKAGF.GameArchitecture.MonoBehaviours.Interaction.Conditions {

    public enum CONDITION_PARAM { GREATER_THAN, LESS_THAN, EQUAL_TO, NOT_EQUAL_TO}

    public class ConditionMonitor : MonoBehaviour {

        [System.Serializable]
        public struct FloatVarCollection {
            public FloatVar floatVar;
            public CONDITION_PARAM conditionParam;
            public float floatValue;

            public bool isConditionSatisfied() {

                switch (conditionParam) {

                    case CONDITION_PARAM.GREATER_THAN: if (floatVar.value > floatValue) return true;
                        break;
                    case CONDITION_PARAM.LESS_THAN: if (floatVar.value < floatValue) return true;
                        break;
                    case CONDITION_PARAM.EQUAL_TO: if(floatVar.value == floatValue) return true;
                        break;
                    case CONDITION_PARAM.NOT_EQUAL_TO: if (floatVar.value != floatValue) return true;
                        break;
                }

                return false;

            }
        }

        [System.Serializable]
        public struct IntVarCollection {
            public IntVar intVar;
            public CONDITION_PARAM conditionParam;
            public int intValue;

            public bool isConditionSatisfied() {

                switch (conditionParam) {

                    case CONDITION_PARAM.GREATER_THAN:
                        if (intVar.value > intValue) return true;
                        break;
                    case CONDITION_PARAM.LESS_THAN:
                        if (intVar.value < intValue) return true;
                        break;
                    case CONDITION_PARAM.EQUAL_TO:
                        if (intVar.value == intValue) return true;
                        break;
                    case CONDITION_PARAM.NOT_EQUAL_TO: if (intVar.value != intValue) return true;
                        break;
                }

                return false;

            }

        }

        [System.Serializable]
        public struct DoubleVarCollection {
            public DoubleVar doubleVar;
            public CONDITION_PARAM conditionParam;
            public double doubleValue;

            public bool isConditionSatisfied() {

                switch (conditionParam) {

                    case CONDITION_PARAM.GREATER_THAN:
                        if (doubleVar.value > doubleValue) return true;
                        break;
                    case CONDITION_PARAM.LESS_THAN:
                        if (doubleVar.value < doubleValue) return true;
                        break;
                    case CONDITION_PARAM.EQUAL_TO:
                        if (doubleVar.value == doubleValue) return true;
                        break;
                    case CONDITION_PARAM.NOT_EQUAL_TO: if (doubleVar.value != doubleValue) return true;
                        break;
                }

                return false;

            }
        }

        [System.Serializable]
        public struct BoolVarCollection {
            public BoolVar boolVar;
            public bool boolValue;

            public bool isConditionSatisfied () {
                return boolVar.value == boolValue;
            }
        }

        [System.Serializable]
        public struct StringVarCollection {
            public StringVar stringVar;
            public string stringValue;

            public bool isConditionSatisfied() {
                return stringVar.value == stringValue;
            }
        }

        [System.Serializable]
        public struct ConditionedConditionCollection {

            public Condition condition;

            public FloatVarCollection[] floatsCollection;
            public IntVarCollection[] intsCollection;
            public DoubleVarCollection[] doublesCollection;
            public BoolVarCollection[] boolsCollection;
            public StringVarCollection[] stringsCollection;
        }

        public bool disabledWhenConditionSatisfied;
        public ConditionedConditionCollection conditionedConditionCollection;
        public UnityEvent OnConditionSatisfied; 

        
        void Update() {

            // Floats checking
            for (int i = 0; i < conditionedConditionCollection.floatsCollection.Length; i++) {

                if (!conditionedConditionCollection.floatsCollection[i].isConditionSatisfied()) return;
            }

            // Ints checking
            for (int i = 0; i < conditionedConditionCollection.intsCollection.Length; i++) {

                if (!conditionedConditionCollection.intsCollection[i].isConditionSatisfied()) return;
            }

            // Doubles checking
            for (int i = 0; i < conditionedConditionCollection.doublesCollection.Length; i++) {

                if (!conditionedConditionCollection.doublesCollection[i].isConditionSatisfied()) return;
            }

            // Bools Checking
            for (int i = 0; i < conditionedConditionCollection.boolsCollection.Length; i++) {

                if (!conditionedConditionCollection.boolsCollection[i].isConditionSatisfied()) return;
            }

            // String checking
            for (int i = 0; i < conditionedConditionCollection.stringsCollection.Length; i++) {

                if (!conditionedConditionCollection.stringsCollection[i].isConditionSatisfied()) return;
            }

            // If the execution of the update methods gets up to here, 
            // it means all var conditions are satisfied, so the global
            // condition must be set to satisfied too.
            conditionedConditionCollection.condition.isSatisfied = true;

            if (OnConditionSatisfied != null)
                OnConditionSatisfied.Invoke();

            if (disabledWhenConditionSatisfied)
                enabled = false;
        }
    }

}

